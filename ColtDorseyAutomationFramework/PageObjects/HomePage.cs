﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColtDorseyAutomationFramework.PageObjects
{
    public class HomePage : BasePage
    {
        public HomePage()
        {
            EnsurePageLoaded();
        }

        private By ByFindHotel = By.Id("frmfindHotel");

        public override void EnsurePageLoaded()
        {
            WaitUntilElementIsVisible(ByFindHotel);
        }

        [FindsBy(How = How.Id, Using = "hotelSearchOneBox")]
        public IWebElement TxtFindAHotel { get; set; }

        [FindsBy(How = How.Id, Using = "checkin")]
        public IWebElement TxtCheckIn { get; set; }

        [FindsBy(How = How.Id, Using = "checkout")]
        public IWebElement TxtCheckOut { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".find_button.cta_button")]
        public IWebElement BtnFindIt { get; set; }

        public SearchResultsPage Search(string findHotel, string checkIn, string checkOut)
        {
            SendText(TxtFindAHotel, findHotel);
            SendText(TxtCheckIn, checkIn);
            SendText(TxtCheckOut, checkOut);
            BtnFindIt.Click();
            return CreateInstance<SearchResultsPage>();
        }
    }
}
