﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColtDorseyAutomationFramework.PageObjects
{
    public class CreditCardsPage : BasePage
    {
        public CreditCardsPage()
        {
            EnsurePageLoaded();
        }

        private By ByCreditCards = By.XPath("//h1[contains(text(), 'CREDIT CARDS')]");

        public override void EnsurePageLoaded()
        {
            WaitUntilElementIsVisible(ByCreditCards);
        }

        [FindsBy(How = How.XPath, Using = "//div[@class='iw_component']//p[contains(text(), 'Hilton Honors  Card')]")]
        public IWebElement LblHiltonHonorsCard { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='iw_component']//p[contains(text(), 'Hilton Honors  Amex Ascend Card')]")]
        public IWebElement LblHiltonHonorsAmexAscendCard { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@class='iw_component']//p[contains(text(), 'Hilton  Honors Aspire Card')]")]
        public IWebElement LblHiltonHonorsAspireCard { get; set; }


        [FindsBy(How = How.XPath, Using = "//div[@class='iw_component']//p[contains(text(), 'Hilton Honors  Business Card')]")]
        public IWebElement LblHiltonHonorsBusinessCard { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[contains(text(), '*Rates and Fees') and contains(@href, 'credit-card') and position() > 1]")]
        public IReadOnlyCollection<IWebElement> LinkRatesAndFees { get; set; }

        //a[contains(text(), '*Rates and Fees') and contains(@href, 'hilton-honors-')]
        public void VerifyCreditCardOffers(string name, string apr)
        {
            JSHighlightElement(LblHiltonHonorsCard);
            JSHighlightElement(LblHiltonHonorsCard);
            JSHighlightElement(LblHiltonHonorsCard);
            JSHighlightElement(LblHiltonHonorsCard);

            for(int i = 0; i < 4; i++)
            {
                LinkRatesAndFees.ElementAt(i).Click();

                CreateInstance<CreditCardRatesPage>().VerifyAPR(apr);
            }
        }

    }
}
