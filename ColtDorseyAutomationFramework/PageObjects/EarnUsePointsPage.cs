﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColtDorseyAutomationFramework.PageObjects
{
    public class EarnUsePointsPage : BasePage
    {
        public EarnUsePointsPage()
        {
            EnsurePageLoaded();
        }

        private By ByEarnUsePoints = By.XPath("//h1[contains(text(), 'Earn & Use Points')]");
        public override void EnsurePageLoaded()
        {
            WaitUntilElementIsVisible(ByEarnUsePoints);
        }

        public class SubNavBar : EarnUsePointsPage
        {
            public SubNavBar()
            {
                EnsurePageLoaded();
            }

            private By BySecondaryNav = By.Id("secondary_nav");

            public override void EnsurePageLoaded()
            {
                WaitUntilElementIsVisible(BySecondaryNav);
            }

            [FindsBy(How = How.LinkText, Using = "Credit Cards")]
            public IWebElement LinkCreditCards { get; set; }

            public CreditCardsPage NavigateToCreditCardsPage()
            {
                LinkCreditCards.Click();
                return CreateInstance<CreditCardsPage>();
            }
        }
    }
}
