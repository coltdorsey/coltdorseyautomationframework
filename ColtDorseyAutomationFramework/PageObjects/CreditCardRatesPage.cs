﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColtDorseyAutomationFramework.PageObjects
{
    public class CreditCardRatesPage : BasePage
    {
        public CreditCardRatesPage()
        {
            EnsurePageLoaded();
        }

        private By ByLblAPR = By.XPath("//p[contains(text(), 'Annual Percentage Rate (APR) for Purchases')]");
        public override void EnsurePageLoaded()
        {
            WaitUntilElementIsVisible(ByLblAPR);
        }

        public void VerifyAPR(string apr)
        {
            
        }
    }
}
