﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using static OpenQA.Selenium.Support.PageObjects.PageFactory;

namespace ColtDorseyAutomationFramework.PageObjects
{
    [Binding]
    public abstract class BasePage
    {
        public static IWebDriver Driver;
        private TimeSpan wdwTime = TimeSpan.FromSeconds(30);
        private TimeSpan iwTime = TimeSpan.FromSeconds(15);

        public abstract void EnsurePageLoaded();

        protected T CreateInstance<T>() where T : BasePage, new()
        {
            T pageClass = new T();

            InitElements(Driver, pageClass);
            pageClass.CheckReadyState();
            return pageClass;
        }

        public void Init(string browser)
        {
            switch (browser.ToLower())
            {
                case "chrome":
                    Driver = new ChromeDriver();
                    break;
                case "firefox":
                    Driver = new FirefoxDriver();
                    break;
                case "ie":
                    Driver = new InternetExplorerDriver();
                    break;
                default:
                    Driver = new ChromeDriver();
                    break;
            }
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitWait = iwTime;
        }

        public IWebElement WaitUntilElementIsVisible(By by)
        {
            return Wait().Until(ExpectedConditions.ElementIsVisible(by));
        }

        public IWebElement WaitUntilElementIsClickable(By by)
        {
            return Wait().Until(ExpectedConditions.ElementToBeClickable(by));
        }

        public IReadOnlyCollection<IWebElement> WaitUntilVisbilityOfAllElementsLocated(By by)
        {
            return Wait().Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(by));
        }

        private WebDriverWait Wait()
        {
            return new WebDriverWait(Driver, wdwTime);
        }

        private void CheckReadyState()
        {
            bool result = false;
            int attempts = 0;

            while (!result && attempts < 5)
            {
                result = JS().ExecuteScript("return document.readyState").Equals("complete");
                if(!result)
                {
                    Thread.Sleep(750);
                    attempts++;
                }
            }
            //Wait().Until(Driver => JS().ExecuteScript("return document.readyState") == "complete");
        }

        private IJavaScriptExecutor JS()
        {
            return (IJavaScriptExecutor)Driver;
        }

        public void SendText(IWebElement element, string text)
        {
            if (element != null)
            {
                element.Clear();
                element.SendKeys(text);
            }
        }

        public void JSHighlightElement(IWebElement element)
        {
            JS().ExecuteScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        }

        public void JSScrollToAndHighlightElement(IWebElement element)
        {
            JSScrollTo(element);
            JSHighlightElement(element);
        }

        public void JSScrollTo(IWebElement element)
        {
            JS().ExecuteScript("arguments[0].scrollIntoView(true);", element);

        }

        public String[] GetNumberFromElementText(IWebElement element)
        {
            return Regex.Split(element.Text, @"\D+").Where(x => !string.IsNullOrEmpty(x)).ToArray();
        }

        public class NavBar : BasePage
        {
            public NavBar()
            {
                EnsurePageLoaded();
            }

            private By ByPrimaryNavList = By.Id("primary_nav_list");

            public override void EnsurePageLoaded()
            {
                WaitUntilElementIsVisible(ByPrimaryNavList);
            }

            [FindsBy(How = How.LinkText, Using = "Points")]
            public IWebElement LinkPoints { get; set; }

            public EarnUsePointsPage NavigateToEarnUsePointsPage()
            {
                LinkPoints.Click();
                return CreateInstance<EarnUsePointsPage>();
            }
        }
    }
}
