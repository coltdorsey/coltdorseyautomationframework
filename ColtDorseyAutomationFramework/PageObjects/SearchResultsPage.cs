﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ColtDorseyAutomationFramework.PageObjects
{
    public class SearchResultsPage : BasePage
    {
        public SearchResultsPage()
        {
            EnsurePageLoaded();
        }

        private By BySearchResults = By.ClassName("matchingNumberOfHotels");
        private By ByFieldSet = By.XPath("//div[@id='hotelsEndlessScrolling']//fieldset");

        public override void EnsurePageLoaded()
        {
            WaitUntilElementIsVisible(BySearchResults);
            WaitUntilVisbilityOfAllElementsLocated(ByFieldSet);
        }

        [FindsBy(How = How.ClassName, Using = "matchingNumberOfHotels")]
        public IWebElement LblResultsNumber { get; set; }

        private void VerifyResultsReturned()
        {
            string[] arr = GetNumberFromElementText(LblResultsNumber);
            
            if (arr.Length < 1 && Convert.ToInt32(arr[0]) < 1)
            {
                Assert.Fail("No search results were displayed.");
            }
        }

        public void VerifySearchResults()
        {
            VerifyResultsReturned();

            ReadOnlyCollection<IWebElement> resultRows = Driver.FindElements(ByFieldSet);
            foreach (var row in resultRows)
            {
                JSScrollToAndHighlightElement(row);
            }
        }
    }
}
