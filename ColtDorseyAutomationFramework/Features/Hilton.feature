﻿Feature: Hilton
	In order get a job at Steady
	As an SDET
	I need to create a framework, automate tests and check it in to source control to be reviewed by the powers that be.

Scenario Outline: Search for popular travel destination
	Given Test executes in <browser>
	When Hilton website is displayed in <environment>
	Then Search for destination
	| findHotel   | arrival     | departure   |
	| atlanta, ga | 29 Oct 2018 | 02 Nov 2018 |
	And Verify search results
	Examples:
	| browser |
	| Chrome  |
	| Firefox |
	| IE	  |

Scenario Outline: View available credit card offers
	Given Test executes in <browser>
	When Hilton website is displayed in <environment>
	Then Navigate to Points section and view available credit card offers
	| cardName                       | apr              |
	| Hilton Honors Card             | 17.49% to 26.49% |
	| Hilton Honors Amex Ascend Card | 17.49% to 26.49% |
	| Hilton Honors Aspire Card      | 17.49% to 26.49% |
	| Hilton Honors Business Card    | 17.49% to 26.49% |
	And Verify search results
	Examples:
	| browser |
	| Chrome  |
	| Firefox |
	| IE	  |

Scenario Outline: View available credit card offers including international cards
	Given Test executes in <browser>
	When Hilton website is displayed in <environment>
	Then Navigate to Points section and view available credit card offers including international
	| cardName                               | aprOrAnnualFee   |
	| Hilton Honors Card                     | 17.49% to 26.49% |
	| Hilton Honors Amex Ascend Card         | 17.49% to 26.49% |
	| Hilton Honors Aspire Card              | 17.49% to 26.49% |
	| Hilton Honors Business Card            | 17.49% to 26.49% |
	| Austria & Germany                      | 48               |
	| Japan Classic Hilton Honors Visa Card  | 3,195            |
	| Japan Gold Hilton Honors Visa Card     | 13,950           |
	| Japan Platinum Hilton Honors Visa Card | 66,500           |
	And Verify search results
	Examples:
	| browser |
	| Chrome  |
	| Firefox |
	| IE	  |