﻿using ColtDorseyAutomationFramework.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace ColtDorseyAutomationFramework.Steps
{
    [Binding]
    public class HiltonSteps : BaseSteps
    {
        [Then(@"Search for destination")]
        public void ThenSearchForDestination(Table table)
        {
            dynamic reservation = table.CreateDynamicInstance();
            CreateInstance<HomePage>().Search(reservation.findHotel.ToString(), reservation.arrival.ToString(), reservation.departure.ToString());
        }

        [Then(@"Verify search results")]
        public void ThenVerifySearchResults()
        {
            CreateInstance<SearchResultsPage>().VerifySearchResults();
        }

        [Then(@"Navigate to Points section and view available credit card offers")]
        public void ThenNavigateToPointsSectionAndViewAvailableCreditCardOffers(Table table)
        {
            dynamic creditCard = table.CreateDynamicInstance();
            CreateInstance<CreditCardsPage>().VerifyCreditCardOffers(creditCard.name.ToString(), creditCard.apr.ToString());
        }

        [Then(@"Navigate to Points section and view available credit card offers including international")]
        public void ThenNavigateToPointsSectionAndViewAvailableCreditCardOffersIncludingInternational(Table table)
        {
            ScenarioContext.Current.Pending();
        }

    }
}
