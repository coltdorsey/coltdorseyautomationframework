﻿using ColtDorseyAutomationFramework.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ColtDorseyAutomationFramework.Steps
{
    [Binding]
    public class CommonSteps : BaseSteps
    {


        [Given(@"Test executes in (.*)")]
        public void GivenTestExecutesInBrowser(string browser)
        {
            Init(browser);
        }

        [When(@"Hilton website is displayed in (.*)")]
        public HomePage WhenHiltonWebsiteIsDisplayedIn(string environment)
        {
            if (string.IsNullOrEmpty(environment))
            {
                environment = Environment;
            }

            switch (environment.ToLower())
            {
                case "development":
                    // Hypothetical dev environment
                    break;
                case "stress":
                    // Hypothetical stress environment
                    break;
                case "production":
                    Driver.Navigate().GoToUrl(Properties.Environment.Default.HiltonProd);
                    break;
                default:
                    Driver.Navigate().GoToUrl(Properties.Environment.Default.HiltonProd);
                    break;
            }

            return CreateInstance<HomePage>();
        }
    }
}
