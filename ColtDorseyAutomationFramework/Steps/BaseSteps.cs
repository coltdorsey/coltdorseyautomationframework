﻿
using ColtDorseyAutomationFramework.PageObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace ColtDorseyAutomationFramework.Steps
{
    [Binding]
    public class BaseSteps : BasePage
    {
        public static string Environment;
        public override void EnsurePageLoaded()
        {
            ///XXX: Dont Implement
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            String[] environmentTags = FeatureContext.Current.FeatureInfo.Tags;

            if (environmentTags.Length > 1)
            {
                Assert.Fail("The frameworok only supports one feature environment tag");
            }
            else if (environmentTags.Length < 1)
            {
                return;
            }
            else if (string.Equals(environmentTags[0], "development", StringComparison.OrdinalIgnoreCase))
            {
                Environment = "development";
            }
            else if (string.Equals(environmentTags[0], "stress", StringComparison.OrdinalIgnoreCase))
            {
                Environment = "stress";
            }
            else if (string.Equals(environmentTags[0], "production", StringComparison.OrdinalIgnoreCase))
            {
                Environment = "production";
            }

        }

        [AfterScenario]
        public void AfterScenario()
        {
            Driver.Quit();
        }
    }
}

